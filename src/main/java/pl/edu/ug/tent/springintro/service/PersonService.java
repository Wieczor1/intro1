package pl.edu.ug.tent.springintro.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import pl.edu.ug.tent.springintro.domain.Person;

@Service
public class PersonService {

  @Autowired
  @Qualifier("prezes")
  Person prezes;

  @Autowired
  @Qualifier("wiceprezes")
  Person wiceprezes;

  @Autowired
  @Qualifier("sekretarka")
  Person sekretarka;

  public Person getPrezes() {
    return prezes;
  }

  public void setPrezes(Person prezes) {
    this.prezes = prezes;
  }

  public Person getWiceprezes() {
    return wiceprezes;
  }

  public void setWiceprezes(Person wiceprezes) {
    this.wiceprezes = wiceprezes;
  }

  public Person getSekretarka() {
    return sekretarka;
  }

  public void setSekretarka(Person sekretarka) {
    this.sekretarka = sekretarka;
  }
}